<?php

/**
 * This is the model class for table "job".
 *
 * The followings are the available columns in table 'job':
 * @property integer $job_id
 * @property integer $client_id
 * @property integer $category
 * @property integer $subcategory
 * @property string  $title
 * @property string  $description
 * @property integer $pay_type
 * @property integer $visibility
 * @property string  $file
 * @property string  $filename
 * @property integer $qualification
 * @property integer $rank
 * @property integer $location
 * @property integer $english_level
 * @property integer $allow_manual_time
 * @property integer $contract_status
 * @property integer $contract_id
 * @property integer $allow_designer_id
 * @property integer $status
 * @property datetime $created_at
 * @property datetime $updated_at
 * @property datetime $start_date
 * @property datetime $delivery_date
 * @property integer $budget
 * @property integer $max_time
 * @property integer $max_time_units
 * @property integer $temp_id
 * @property integer $paid_status
 * @property string $release_budget
 */
class Job extends CActiveRecord {

    const STATUS_DRAFT = 0; //Client saved job as draft
    const STATUS_OPEN = 1; //Client is looking for designers
    const STATUS_COMPLETED = 2; //Job completed and storing for history
    const STATUS_CLOSED = 3; //Job closed. Probably it's not necessary
    const STATUS_DISABLED = 6; //Job disabled. Probably it's not necessary
    const STATUS_INWORK = 4; //Designer has been selected. Job is in work
    const STATUS_WAITED = 5; // Client send Proposal to designer

    const CONTRACT_STATUS_OPEN = 0; //Client haven't choose designer yet
    const CONTRACT_STATUS_STARTED = 1; //Designer is working with job
    const CONTRACT_STATUS_PAUSED = 2; //Client paused work and designer isn't able to track time
    const CONTRACT_STATUS_STOPPED = 3; //Client stopped job
    const CONTRACT_STATUS_COMPLETED = 4; //Client stopped job

    /* Categories */
    const REMODELING_CATEGORY = 1;
    const INTERIOR_CATEGORY = 2;
    const LANDSCAPE_CATEGORY = 3;
    const RESIDENTIAL_CATEGORY = 4;
    const PUBLIC_COMMERCIAL_CATEGORY = 5;

    /* Pay types */
    const PAY_HOURLY = 0;
    const PAY_FIXED = 1;

    /* Visibility types */
    const GOOGLE_VISIBLE = 1;
    const GOOGLE_INVISIBLE = 0;
    const ALL_INVISIBLE = 2;

    /* Qualification */
    const NO_PREFERENCE = 0;
    const LOW_QUALIFICATION = 1;
    const HIGH_QUALIFICATION = 2;
    const LICENSED_QUALIFICATION = 3;

    /* Ranks */
    const ANY_RANK = 0;
    const TEN_RANK = 1;
    const FIFTY_RANK = 2;
    const HUNDRED_RANK = 3;
    const THOUSAND_RANK = 4;

    /* Locations */
    const ANY_LOCATION = 0;
    const LOCAL_STATE = 1;
    const NORTH_AMERICA = 2;
    const SOUTH_AMERICA = 3;
    const EUROPE = 4;
    const AFRICA = 5;
    const MIDDLE_EAST = 6;
    const EAST_ASIA = 7;
    const OCEANIA = 8;

    /* English level */
    const ANY_LEVEL = 0;
    const LOW_LEVEL = 1;
    const AVERAGE_LEVEL = 2;
    const HIGH_LEVEL = 3;
    const NATIVE_SPEAKER = 4;

    const STATUS_NOT_PAID = 0;
    const STATUS_PAID = 1;

    public $timeFromPosting;
    public $doc;
    public $thumb;
    public $cond;

    const MAX_TIME_HOURS = 0;
    const MAX_TIME_WEEKS = 1;

    public static function getMaxTimeUnits($unit = false){
        $locations = array(
            self::MAX_TIME_HOURS => 'Hours',
            self::MAX_TIME_WEEKS => 'Weeks',
        );
        if(false !== $unit){
            return $locations[$unit];
        } else {
            return $locations;
        }
    }

    public static function getStatus($status = false){
        $statuses = array(
            self::STATUS_DRAFT => 'Saved as Draft',
            self::STATUS_OPEN => 'Open',
            self::STATUS_COMPLETED => 'Completed',
            self::STATUS_CLOSED => 'Closed',
            self::STATUS_DISABLED => 'Disabled',
            self::STATUS_INWORK => 'In Work',
            self::STATUS_WAITED => 'Waited',
        );
        if(false !== $status){
            return $statuses[$status];
        } else {
            return $statuses;
        }
    }

    public static function getContractStatus2($status = false){
        $statuses = array(
            self::CONTRACT_STATUS_OPEN => 'No Contract yet',
            self::CONTRACT_STATUS_STARTED => 'Started',
            self::CONTRACT_STATUS_PAUSED => 'Paused',
            self::CONTRACT_STATUS_STOPPED => 'Stopped',
            self::CONTRACT_STATUS_COMPLETED => 'Completed',
        );
        if(false !== $status){
            return $statuses[$status];
        } else {
            return $statuses;
        }
    }

    protected function afterFind()
    {
        parent::afterFind();
        $this->timeFromPosting = time() - $this->created_at;
        if ($this->timeFromPosting < 3600 ){
            $this->timeFromPosting = Yii::t('front', 'less than hour');
        } elseif ($this->timeFromPosting < 86400 ){
            $this->timeFromPosting = round($this->timeFromPosting / 3600) . ' hour(s)';
        } elseif($this->timeFromPosting < 2592000 ){
            $this->timeFromPosting = round($this->timeFromPosting / 86400) . ' day(s)';
        } else {
            $this->timeFromPosting = round($this->timeFromPosting / 2592000) . ' month(s)';
        }
    }

    public static function getEnglishLevel($loc = false){
        $locations = array(
            self::ANY_LEVEL => 'Any Level',
            self::LOW_LEVEL => 'Low Level',
            self::AVERAGE_LEVEL => 'Average Level',
            self::HIGH_LEVEL => 'High Level',
            self::NATIVE_SPEAKER => 'Native Speaker',
        );
        if(false !== $loc){
            return $locations[$loc];
        } else {
            return $locations;
        }
    }

    public static function getLocation($loc = false){
        $locations = array(
            self::ANY_LOCATION => 'Any Location',
            self::LOCAL_STATE => 'Local State',
            self::NORTH_AMERICA => 'North America',
            self::SOUTH_AMERICA => 'South America',
            self::EUROPE => 'Europe',
            self::AFRICA => 'Africa',
            self::MIDDLE_EAST => 'Middle-East',
            self::EAST_ASIA => 'East Asia',
            self::OCEANIA => 'Oceania',
        );
        if(false !== $loc){
            return $locations[$loc];
        } else {
            return $locations;
        }
    }

    public static function getRank($rank = false){
        $ranks = array(
            self::ANY_RANK => 'Any Rank',
            self::TEN_RANK => 'Top 10',
            self::FIFTY_RANK => 'Top 50',
            self::HUNDRED_RANK => 'Top 100',
            self::THOUSAND_RANK => 'Top 1000',
        );
        if(false !== $rank){
            return $ranks[$rank];
        } else {
            return $ranks;
        }
    }

    public static function getQualification($level = false){
        $qualifications = array(
            self::NO_PREFERENCE => 'No preference',
            self::LOW_QUALIFICATION => '1-5 experience',
            self::HIGH_QUALIFICATION => '5+ experience',
            self::LICENSED_QUALIFICATION => 'Licensed in State',
        );
        if(false !== $level){
            return $qualifications[$level];
        } else {
            return $qualifications;
        }
    }

    public static function getVisibility($type = false){
        $types = array(
            self::GOOGLE_VISIBLE => 'Give my job maximum exposure (people can find it on google)',
            self::GOOGLE_INVISIBLE => 'Only Arcbazar users can find this job',
            self::ALL_INVISIBLE => 'Only people I have invited can find this job',
        );
        if($type){
            return $types[$type];
        } else {
            return $types;
        }
    }
    public static function getVisibilityPost($type = false){
        $types = array(
            self::GOOGLE_VISIBLE => 'Give my job maximum exposure (people can find it on google)',
            self::GOOGLE_INVISIBLE => 'Only Arcbazar users can find this job',
//            self::ALL_INVISIBLE => 'Only people I have invited can find this job',
        );
        if($type){
            return $types[$type];
        } else {
            return $types;
        }
    }

    public static function getVisibility2($type = false){
        switch($type){
            case self::GOOGLE_VISIBLE:
                return 'Public';
                break;
            case self::GOOGLE_INVISIBLE:
                return 'Open';
                break;
            case self::ALL_INVISIBLE:
                return 'Invite-only';
                break;
            default:
                return '';
        }
    }

    public static function getVisibility3($type = false){
        switch($type){
            case self::GOOGLE_VISIBLE:
            case self::GOOGLE_INVISIBLE:
                return 'Public post';
                break;
            case self::ALL_INVISIBLE:
                return 'Private post';
                break;
            default:
                return '';
        }
    }

    public static function getVisibility4($type = false){
        $types = array(
            self::GOOGLE_INVISIBLE => Yii::t('front', 'Make this job open to all designers'),
            self::ALL_INVISIBLE => Yii::t('front', 'Make this job open only to the following designers'),
        );
        if($type){
            if($type == self::GOOGLE_VISIBLE) $type = self::GOOGLE_INVISIBLE;
            return $types[$type];
        } else {
            return $types;
        }
    }

    public static function getPayType($type = false){
        $types = array(
            self::PAY_HOURLY => 'Hourly. Pay by The hour. Verify with the Work Diary.',
            self::PAY_FIXED => 'Fixed Price – Pay by the project. Requires detailed specs',
        );
        if(false !== $type){
            return $types[$type];
        } else {
            return $types;
        }
    }

    public static function getPayType2($type = false){
        switch($type){
            case self::PAY_HOURLY:
                return 'Hourly';
                break;
            case self::PAY_FIXED:
                return 'Fixed Price';
                break;
            default:
                return '';
        }
    }

    public static function getPayType3($type = false){
        $types = array(
            self::PAY_HOURLY => 'Hourly',
            self::PAY_FIXED => 'Fixed-Price',
        );
        if(false !== $type){
            return $types[$type];
        } else {
            return $types;
        }
    }

    public static function getPayType4($type = false){
        $types = array(
            self::PAY_HOURLY => 'hour',
            self::PAY_FIXED => 'project',
        );
        if(false !== $type){
            return $types[$type];
        } else {
            return $types;
        }
    }

    public static function getCategories($cat = false) {
        $cats = array(
            self::REMODELING_CATEGORY => 'Remodeling',
            self::INTERIOR_CATEGORY => 'Interior Design',
            self::LANDSCAPE_CATEGORY => 'Landscape Design',
            self::RESIDENTIAL_CATEGORY => 'New Residential',
            self::PUBLIC_COMMERCIAL_CATEGORY => 'Public/Commercial',
        );

        if(false !== $cat) return $cats[$cat];

        return $cats;
    }

    public static function getSubCategories($cat = false, $subCat = false) {

        switch ($cat) {
            case self::REMODELING_CATEGORY:
                $subCats = array(
                    1  => 'Kitchen',
                    2  => 'Bathroom',
                    3  => 'Basement',
                    4  => 'Attic',
                    5  => 'Bedroom',
                    6  => 'Living Room',
                    7  => 'Home Office',
                    8  => 'Kids Room',
                    9  => 'Dining Room',
                    10 => 'Sunroom',
                    11 => 'Porch',
                    12 => 'Garage',
                    13 => 'Façade',
                    14 => 'Additions',
                    15 => 'Other',
                );
                break;
            case self::INTERIOR_CATEGORY:
                $subCats = array(
                    1  => 'Kitchen',
                    2  => 'Bathroom',
                    3  => 'Basement',
                    4  => 'Attic',
                    5  => 'Bedroom',
                    6  => 'Living Room',
                    7  => 'Home Office',
                    8  => 'Kids Room',
                    9  => 'Dining Room',
                    10 => 'Other',
                );
                break;
            case self::LANDSCAPE_CATEGORY:
                $subCats = array(
                    1  => 'Front yard/Back yard',
                    2  => 'Decks/Patios',
                    3  => 'Curb Appeal',
                    4  => 'Pools/Ponds',
                    5  => 'Fireplaces/Open Kitchens',
                    6  => 'Playgrounds',
                    7  => 'Green Houses',
                    8  => 'Paths/Pavings',
                    9  => 'Parking/Carports',
                    10 => 'Other',
                );
                break;
            case self::RESIDENTIAL_CATEGORY:
                $subCats = array(
                    1  => 'Additions',
                    2  => 'Guest Houses/Cabins',
                    3  => 'Single Family Homes',
                    4  => 'Multiple Family Homes',
                    5  => 'Other',
                );
                break;
            case self::PUBLIC_COMMERCIAL_CATEGORY:
                $subCats = array(
                    1  => 'Office Buildings',
                    2  => 'Retail/Small Businesses',
                    3  => 'Restaurants/Bars',
                    4  => 'Healthcare',
                    5  => 'Community/Social',
                    6  => 'Religious',
                    7  => 'Educational/Research',
                    8  => 'Industrial',
                    9  => 'Agricultural',
                    10 => 'Entertainment',
                    11 => 'Transport',
                    12 => 'Hotels/Accommodation',
                    13 => 'Sports/Recreation',
                    14 => 'Animal Related',
                    15 => 'Other',
                );
                break;
            default :
                $subCats = array();
        }
        if(false !== $subCat){
            return $subCats[$subCat];
        } else {
            return $subCats;
        }
    }

    public function getJobStatus($status = false){
        $statuses = array(
            self::STATUS_DRAFT => 'Saved to draft',
            self::STATUS_OPEN => 'Open',
            self::STATUS_COMPLETED => 'Completed',
            self::STATUS_CLOSED => 'Closed',
            self::STATUS_DISABLED => 'Disabled',
        );

        if ($status){
            return $statuses[$status];
        } else {
            return $statuses;
        }
    }

    public function getContractStatus($status = false){
        $statuses = array(
            self::CONTRACT_STATUS_OPEN => 'No contract',
            self::CONTRACT_STATUS_STARTED => 'Job started',
            self::CONTRACT_STATUS_PAUSED => 'Job paused',
            self::CONTRACT_STATUS_STOPPED => 'Job stopped',
            self::CONTRACT_STATUS_COMPLETED => 'Job completed',
        );

        if ($status){
            return $statuses[$status];
        } else {
            return $statuses;
        }
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Job the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'job';
    }

    public function primaryKey() {
        return array('job_id');
    }

    protected function beforeValidate() {
        return parent::beforeValidate();
    }

    protected function beforeSave() {
        return parent::beforeSave();
    }

    /**
     * @return array validation rules for model attributes.
     */

    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('category, subcategory, title, description, budget, status, client_id', 'required' ),
            array('title', 'length', 'max' => 50),
            array('budget', 'numerical', 'integerOnly' => false),
            array('file, filename, budget', 'length', 'max' => 255),
            array('doc, cond', 'file', 'allowEmpty'=>true, 'types'=>'doc,pdf,docx,txt,xlsx,doc,xls,png,jpg,gif,rtf,jpeg,odt,ods,csv', 'maxSize' => 5242880, 'tooLarge' => 'File should be less than 5MB'),
            array('cond', 'file', 'allowEmpty'=>true, 'types'=>'doc,pdf,docx,txt,xlsx,doc,xls,png,jpg,gif,rtf,jpeg,odt,ods,csv'),
            array('thumb', 'file', 'allowEmpty'=>true, 'types'=>'png,jpg,gif,jpeg'),
            array('release_budget, paid_status, temp_id, firstname, desFirstname, allow_designer_id, extra_cond, budget, max_time, max_time_units, start_date, delivery_date, pay_type, visibility, qualification, rank, location, english_level, category, subcategory, title, description, file, filename', 'safe'),
        );
    }
    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'question' => array(self::HAS_MANY, 'JobQuestion', 'job_id',),
            'filesMid' => array(self::HAS_MANY, 'JobFiles', 'job_id', 'on'=>'type=' . JobFiles::FIRST_MID,),
            'filesFin' => array(self::HAS_MANY, 'JobFiles', 'job_id', 'on'=>'type=' . JobFiles::FULL_JOB,),
            'applicants' => array(self::HAS_MANY, 'JobDesigners', 'job_id',),
            'aInvitedDesigners' => array(self::HAS_MANY, 'InvDes', 'job_id',),
            'aInvitedMailDesigners' => array(self::HAS_MANY, 'InvMailDes', 'job_id',),
            'aInvitedDesignersTemp' => array(self::HAS_MANY, 'InvDes', 'temp_id',),
            'aInvitedMailDesignersTemp' => array(self::HAS_MANY, 'InvMailDes', 'temp_id',),
            'client' => array(self::BELONGS_TO, 'User', 'client_id',),
            'allowDesigner' => array(self::BELONGS_TO, 'User', 'allow_designer_id',),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        $labels = array(
            'job_id' => 'ID',
            'client_id' => 'Client ID',
            'title' => 'Title',
            'description' => 'Description',
            'allow_manual_time' => 'Allow manual time on this contract',
        );

        return $labels;
    }

    public $lastname;
    public $desLastname;
    public $firstname;
    public $desFirstname;

    public function search() {

        $criteria = new CDbCriteria;

//        $criteria->with = 'question';
//        $criteria->with = 'applicants';
//        $criteria->together = true;
        $criteria->with = array( 'client', 'allowDesigner');
        $criteria->compare('t.job_id', $this->job_id);
        $criteria->compare('t.title', $this->title, true);
        $criteria->compare('t.category', $this->category, true);
        $criteria->compare('t.status', $this->status, true);
        $criteria->compare('t.client_id', $this->client_id, true);
        $criteria->compare('t.allow_designer_id', $this->allow_designer_id, true);
        $criteria->compare('client.name', $this->firstname, true);
        $criteria->compare('allowDesigner.name', $this->desFirstname, true);
        $criteria->compare('client.lastname', $this->lastname, true);
        $criteria->compare('allowDesigner.lastname', $this->desLastname, true);

//        $criteria->order = 't.created_at desc';
        $sort = new CSort();
        $sort->defaultOrder = 't.created_at desc';
        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => '5',
            ),
        ));

    }

    public static function getDateAgo($date){
        $days = (time() - $date)/3600;

        if($days < 1){
            $days = ' less than 1 hour ';
        } elseif($days < 24) {
            $days = ' less than ' . ceil($days) . ' hours ';
        } else {
            $days = ' less than ' . ceil($days/24). ' days ';
        }

        return $days;
    }

    public function location(){
        if($this->location == self::LOCAL_STATE){
            $location = '';
            if($this->client->geo_city !== NULL){
                $location .= $this->client->geo_city;
            }
            if($this->client->geo_region !== NULL){
                $location .=  ', ' . User::getRegion($this->client->geo_region);
            }
            $location .= ', ' . $this->client->geo_country;
            return $location;
        } else {
            return self::getLocation($this->location);
        }
    }

}
