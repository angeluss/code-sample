<?php
class JobController extends Controller {

    public $layout = 'singleColumn';

    public function actionIndex(){
        if (User::isDesigner()){
            throw new CHttpException(404);
        }
        $criteria = new CDbCriteria();
        $criteria->condition = 'client_id=:client_id AND (contract_status=:started OR contract_status=:paused)';
        $criteria->params = array(
            ':client_id' => Yii::app()->user->getId(),
            ':started' => Job::CONTRACT_STATUS_STARTED,
            ':paused' => Job::CONTRACT_STATUS_PAUSED,
        );
        $criteria->order = 'created_at DESC';
        $criteria->limit = 4;

        $contracts = Job::model()->findAll($criteria);
        $endedContracts = Job::model()->findAllByAttributes(
            array(
                'client_id' => Yii::app()->user->getId(),
                'contract_status' => Job::CONTRACT_STATUS_COMPLETED,
            ),
            array(
                'order' => 'created_at DESC',
                'limit' => 4,
            )
        );
        $jobs = Job::model()->findAllByAttributes(
            array(
                'client_id' => Yii::app()->user->getId(),
                'contract_status' => Job::CONTRACT_STATUS_OPEN,
            ),
                array(
                    'order' => 'created_at DESC',
                    'limit' => 4,
                )
            );
        Yii::app()->clientScript->registerScriptFile('/js/job.js');
        $this->render('index', array(
            'jobs' => $jobs,
            'contracts' => $contracts,
            'endedContracts' => $endedContracts,
        ));
    }

    public function actionLogTime() {
        if(Yii::app()->getRequest()->isAjaxRequest) {
            $success = false;
            $time = Yii::app()->request->getParam('time', false);
            $comment = Yii::app()->request->getParam('comment', false);
            $contract = Yii::app()->request->getParam('contract', false);
            $cont = Contracts::model()->findByPk($contract);
            $job = Job::model()->findByPK($cont->job_id);
            $date = Yii::app()->request->getParam('date', false);
            $date = date("Y-m-d H:i", strtotime($date));
            $timetrack = new TimeTrack();
            $timetrack->contract_id = $contract;
            $timetrack->time = $time;
            $timetrack->comment = $comment;
            $timetrack->updated_at = $date;
            if($timetrack->updated_at == '') {
                $timetrack->updated_at = date("Y-m-d H:i");
            }
            if($timetrack->validate()){
                $timetrack->save();
                $success = true;
            }
            $timetracks = TimeTrack::model()->findAllByAttributes(array('contract_id' => $contract));
            $html = $this->renderPartial('_timetracks', array('timetracks' => $timetracks, 'job' => $job), true);
            echo CJSON::encode(
                array(
                    'success' => $success,
                    'html' => $html,
                ));
            Yii::app()->end();
        } else {
            throw new CHttpException(404);
        }
    }

    public function actionDeleteTime(){
        if(Yii::app()->getRequest()->isAjaxRequest) {
            $success = false;
            $id = Yii::app()->request->getParam('id', false);
            $item = TimeTrack::model()->findByPk($id);
            if($item->delete()){
                $success = true;
            }
            echo CJSON::encode(
                array(
                    'success' => $success,
                ));
            Yii::app()->end();
        } else {
            throw new CHttpException(404);
        }
    }

    public function actionPauseContract($id){
        $job = Job::model()->findByPk($id);
        if(isset($_POST['Job'])){
            if($job->contract_status == Job::CONTRACT_STATUS_PAUSED) {
                $job->contract_status = Job::CONTRACT_STATUS_STARTED;
                $job->pause_reason = '';
                $message = "Contract successfully resumed!";
            } else {
                $job->contract_status = Job::CONTRACT_STATUS_PAUSED;
                $job->pause_reason = $_POST['Job']['pause_reason'];
                $message = "Contract successfully paused!";
            }
            if($job->validate()){
                if($job->save()){
                    Yii::app()->user->setFlash('success', $message);
                    $this->redirect('/job');
                }
            }
        }
        $params = User::getRegionParams($job->allowDesigner->geo_region);

        $date = User::getTimeSpan($params);
        $render = $job->contract_status == Job::CONTRACT_STATUS_PAUSED ? 'resumeContract' : 'pauseContract';
        $this->render($render, array(
            'job' => $job,
            'date' => $date,
        ));
    }

    public function actionEndContract($id){

        $job = Job::model()->findByPk($id);
        $end = new EndContract();
        $params = User::getRegionParams($job->allowDesigner->geo_region);

        $date = User::getTimeSpan($params);
        if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getParam('ajax') == 'endContract-form') {
            echo CActiveForm::validate($end);
            Yii::app()->end();
        }
        if(isset($_POST['EndContract'])){
            $end->attributes = $_POST['EndContract'];
            $end->designer_id = $job->allow_designer_id;
            $job->contract_status = Job::CONTRACT_STATUS_COMPLETED;
            $job->status = Job::STATUS_COMPLETED;
            $account = Account::model()->findByAttributes(array('designer_id' => $job->allow_designer_id));
            if($account === null){
                $account = new Account;
                $account->designer_id = $job->allow_designer_id;
                $account->amount = 0;
            }
            $account->amount += $job->release_budget;

            if($end->validate() && $account->validate()){
                if($end->save()){
                    $job->save();
                    $account->save();
                    Yii::app()->user->setFlash('success', "Contract successfully ended!");
                    $this->redirect('/job');
                }
            }
        }
        $this->render('endContract', array(
            'job' => $job,
            'end' => $end,
            'date' => $date,
        ));
    }

    public function actionReleaseFundsMid($id) {
        if (!User::isClient()){
            throw new CHttpException(404);
        }
        $job = Job::model()->findByPk($id);
        $account = Account::model()->findByAttributes(array('designer_id' => $job->allow_designer_id));
        if($account === null){
            $account = new Account;
            $account->designer_id = $job->allow_designer_id;
            $account->amount = 0;
        }
        $job->release_budget /= 2;
        $job->mid_status = 1;
        $account->amount += $job->release_budget;
        if($job->validate() && $account->validate()){
            $account->save();
            $job->save();
            Yii::app()->user->setFlash('success', "Funds successfully released!");
            $this->actionApplicants($id);
        } {
            CVarDumper::dump($job->getErrors());
            CVarDumper::dump($account->getErrors());
        }

    }

    public function actionAskReleaseFundsMid($id) {
        if (!User::isDesigner()){
            throw new CHttpException(404);
        }
        $job = Job::model()->findByPk($id);
        $to = $job->client->email;
        $sublect = 'ask release funds for ' . $job->title . '(mid milestone)';
        $message = $job->allowDesigner->displayName . ' uploaded files of the mid milestone. Please release funds for him';
        $this->sendEmail($to, $sublect, $message);
        Yii::app()->user->setFlash('success', "Request successfully sent!");
        $this->redirect('/watson/job/workRoom/'. $id);
    }

    public function actionAskReleaseFundsFin($id) {
        if (!User::isDesigner()){
            throw new CHttpException(404);
        }
        $job = Job::model()->findByPk($id);
        $to = $job->client->email;
        $sublect = 'ask release funds for ' . $job->title . 'final';
        $message = $job->allowDesigner->displayName . ' uploaded files. Please release funds for him and end the contract';
        $this->sendEmail($to, $sublect, $message);
        Yii::app()->user->setFlash('success', "Request successfully sent!");
        $this->redirect('/watson/job/workRoom/'. $id);
    }

    public function actionAllowManualTime($id){
        $job = Job::model()->findByPk($id);
        if(isset($_POST['Job'])){
            $job->allow_manual_time = $_POST['Job']['allow_manual_time'];
            if($job->validate()){
                if($job->save()){
                    Yii::app()->user->setFlash('success', "Allow time changed!");
                    $this->redirect('/job');
                }
            }
        }
        $params = User::getRegionParams($job->allowDesigner->geo_region);

        $date = User::getTimeSpan($params);
        $this->render('allowManual', array(
            'job' => $job,
            'date' => $date,
        ));
    }

    public function actionOverView($id){
        $job = Job::model()->findByPk($id);
        $contract = Contracts::model()->findByAttributes(array('job_id' =>$id));
        $timetracks = TimeTrack::model()->findAllByAttributes(array('contract_id' => $contract->contract_id));
        $params = User::getRegionParams($job->allowDesigner->geo_region);
        $chat = Chat::model()->findByAttributes(array('job_id' => $id));

        if(is_null($chat)){
            $chat = new Chat;
            $chat->client_id = $job->client_id;
            $chat->designer_id = $job->allow_designer_id;
            $chat->job_id = $id;
            $chat->updated_at = time();
            if($chat->validate()){
                $chat->save();
            } else{
                $chat->lastMessage = null;
            }
        }
        $date = User::getTimeSpan($params);
        $lastWeek = TimeTrack::getTimeSum($contract->contract_id, TimeTrack::LAST_WEEK);
        $last_week_budget = TimeTrack::getBudget($lastWeek, $job->budget);
        $thisWeek = TimeTrack::getTimeSum($contract->contract_id, TimeTrack::THIS_WEEK);
        $this_week_budget = TimeTrack::getBudget($thisWeek, $job->budget);
        $alltimes = TimeTrack::getTimeSum($contract->contract_id, TimeTrack::ALL_TIME);
        $all_time_budget = TimeTrack::getBudget($alltimes, $job->budget);
        $invoice = Invoice::model()->findByAttributes(array('job_id' => $id));

        if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getParam('ajax') == 'changePerson-form') {
            echo CActiveForm::validate($contract);
            Yii::app()->end();
        }
            if(isset($_POST['Contracts']) && isset($_POST['submit'])){
                $contract->attributes = $_POST['Contracts'];
                if($contract->validate()){
                    $contract->save();
                }
            }
        Yii::app()->clientScript->registerScriptFile('/js/job.js');
        Yii::app()->clientScript->registerScriptFile('/js/timetrack.js');
        $this->render('overview', array(
            'job' => $job,
            'contract' => $contract,
            'date' => $date,
            'timetracks' => $timetracks,
            'thisWeek' => $thisWeek,
            'lastWeek' => $lastWeek,
            'alltimes' => $alltimes,
            'all_time_budget' => $all_time_budget,
            'this_week_budget' => $this_week_budget,
            'last_week_budget' => $last_week_budget,
            'chat' => $chat,
            'invoice' => $invoice,
        ));
    }
    public function actionWorkDiary($id){

        $job = Job::model()->findByPk($id);
        $contract = Contracts::model()->findByAttributes(array('job_id' =>$id));
        $timetracks = TimeTrack::model()->findAllByAttributes(array('contract_id' => $contract->contract_id));

        $params = User::getRegionParams($job->allowDesigner->geo_region);
        $date = User::getTimeSpan($params);
        Yii::app()->clientScript->registerScriptFile('/js/timetrack.js');
        Yii::app()->clientScript->registerScriptFile('/js/job.js');
        $this->render('workDiary', array(
            'job' => $job,
            'contract' => $contract,
            'date' => $date,
            'timetracks' => $timetracks,
        ));
    }

    public function actionContractHistory($id){
        $job = Job::model()->findByPk($id);
        $contract = Contracts::model()->findByAttributes(array('job_id' =>$id));
        $params = User::getRegionParams($job->allowDesigner->geo_region);
        $date = User::getTimeSpan($params);
        $invoice = Invoice::model()->findByAttributes(array('job_id' => $id));
        $alltimes = TimeTrack::getTimeSum($contract->contract_id, TimeTrack::ALL_TIME);
        $all_time_budget = TimeTrack::getBudget($alltimes, $job->budget);
        $this->render('contractHistory', array(
            'job' => $job,
            'contract' => $contract,
            'date' => $date,
            'invoice' => $invoice,
            'all_time_budget' => $all_time_budget,
        ));
    }

    public function actionViewInvoice ($id, $print = false){
        $invoice = Invoice::model()->findByPk($id);
        $job = Job::model()->findByPk($invoice->job_id);
        $contract = Contracts::model()->findByAttributes(array('job_id' => $job->job_id));
        $alltimes = TimeTrack::getTimeSum($contract->contract_id, TimeTrack::ALL_TIME);
        $all_time_budget = TimeTrack::getBudget($alltimes, $job->budget);
        $params = User::getRegionParams($job->allowDesigner->geo_region);
        $date = User::getTimeSpan($params);
        if($print){
            Yii::app()->clientScript->reset();
            $html = '<h2>' . Yii::t('front', 'Invoice #') . $id . '</h2>';
            $html .= $this->renderPartial(
                'printInvoice',
                array(
                    'job' => $job,
                    'contract' => $contract,
                    'alltimes' => $alltimes,
                    'all_time_budget' => $all_time_budget,
                ),
                true,
                false
            );
            ExportService::exportDetailPdf($html, 'invoice_' . $id . '.pdf', true);
            Yii::app()->end();
        }
        $this->render('viewInvoice', array(
            'job' => $job,
            'contract' => $contract,
            'invoice' => $invoice,
            'date' => $date,
            'alltimes' => $alltimes,
            'all_time_budget' => $all_time_budget,
        ));
    }

    public function actionDeleteInvoice($id){
        $invoice = Invoice::model()->findByPk($id);
        $id = $invoice->job_id;
        if(is_null($invoice)){
            throw new CHttpException(404);
        }
        if($invoice->delete()){
            $this->redirect(Yii::app()->createUrl('/job/contractHistory', array('id' => $id)));
        }
    }

    public function actionDeclineHire($job_id){
        $job = Job::model()->findByPk($job_id);
        $job->status = Job::STATUS_OPEN;
        $job->allow_designer_id = NULL;
        if($job->save()){
            $this->redirect(array('/job'));
        }
    }

    public function actionMakePublic($id){
        $job = Job::model()->findByPk($id);
        $job->visibility = Job::GOOGLE_VISIBLE;
        if($job->validate()){
            $job->save();
            $this->redirect(array('/job'));
        } else {
            throw new CHttpException(404);
        }
    }

    public function actionPreview($job){

        $job = Job::model()->findByPk($job);

        if (empty($job)){
            throw new CHttpException(404);
        }
        if($job->client_id != Yii::app()->user->getId()) {
            throw new CHttpException(404);
        }
        Yii::app()->clientScript->registerMetaTag('noindex', 'googlebot');
        $this->render('preview', array(
            'job' => $job,
        ));
    }

    public function actionDeleteQuestion(){
        if(Yii::app()->getRequest()->isAjaxRequest) {
            $id = Yii::app()->request->getParam('id', false);
            $question = JobQuestion::model()->findByPk($id);
            if(!empty($question)){
                $question->delete();
            }
            echo CJSON::encode(
                array(
                    'success' => true,
                ));
            Yii::app()->end();
        } else {
            throw new CHttpException(404);
        }
    }
    public function actionMyJobs(){
        if (User::isDesigner()){
            $jobs = Job::model()->findAllByAttributes(array(
                'status' => Job::STATUS_INWORK,
                'allow_designer_id' => Yii::app()->user->getId(),
                'contract_status' => Job::CONTRACT_STATUS_STARTED,
            ));
            $closedJobs = Job::model()->findAllByAttributes(array(
                'status' => Job::STATUS_INWORK,
                'allow_designer_id' => Yii::app()->user->getId(),
                'contract_status' => Job::CONTRACT_STATUS_COMPLETED,
            ));
            $model = User::model()->findByPk(Yii::app()->user->getId());
            Yii::app()->clientScript->registerCssFile('/styles/dev.css');
            Yii::app()->clientScript->registerScriptFile('/js/job.js');
            $this->render('designerMyJobs', array(
                'jobs' => $jobs,
                'closedJobs' => $closedJobs,
                'model' => $model,
            ));
        } else {
            throw new CHttpException(404);
        }
    }

    public function actionMyHome($category = false){
        if (User::isDesigner()){
            $criteria = new CDbCriteria();
            $criteria->condition = 'status=:status AND visibility<>:visibility';
            $criteria->params = array(':status' => Job::STATUS_OPEN, ':visibility' => Job::ALL_INVISIBLE);
            if($category){
                $criteria->condition .= ' AND category=:category';
                $criteria->params[':category'] = $category;
            }
            $criteria->order = 'created_at desc';
            $count = Job::model()->count($criteria);

            $pages = new CPagination($count);
            $pages->pageSize = 5;
            $pages->applyLimit($criteria);

            $jobs = Job::model()->findAll($criteria);

            $criteria = new CDbCriteria();
            $criteria->condition = 'status=:status AND allow_designer_id=:des_id';
            $criteria->params = array(':status' => Job::STATUS_WAITED, ':des_id' => Yii::app()->user->getId());
            $criteria->order = 'created_at desc';
            $proposals = Job::model()->findAll($criteria);
            $offers = $proposals;
            Yii::app()->clientScript->registerCssFile('/styles/dev.css');
            Yii::app()->clientScript->registerScriptFile('/js/job.js');
            $this->render('designerMyHome', array(
                'jobs' => $jobs,
                'offers' => $offers,
                'pages' => $pages,
                'proposals' => $proposals,
            ));
        } else {
            throw new CHttpException(404);
        }
    }

    public function actionMyOffers($category = false){
        if (User::isDesigner()){
            $criteria = new CDbCriteria();
            $criteria->condition = 'status=:status AND visibility<>:visibility';
            $criteria->params = array(':status' => Job::STATUS_OPEN, ':visibility' => Job::ALL_INVISIBLE);
            if($category){
                $criteria->condition .= ' AND category=:category';
                $criteria->params[':category'] = $category;
            }
            $criteria->order = 'created_at desc';
            $count = Job::model()->count($criteria);

            $pages = new CPagination($count);
            $pages->pageSize = 5;
            $pages->applyLimit($criteria);

            $criteria = new CDbCriteria();
            $criteria->condition = 'status=:status AND allow_designer_id=:des_id';
            $criteria->params = array(':status' => Job::STATUS_WAITED, ':des_id' => Yii::app()->user->getId());
            $criteria->order = 'created_at desc';
            $proposals = Job::model()->findAll($criteria);
            $invites = InvDes::model()->findAllByAttributes(array(
                'designer_id' => Yii::app()->user->getId(),
            ));
            $user = User::model()->findByPk(Yii::app()->user->getId());
            $mailinvites = InvMailDes::model()->findAllByAttributes(array(
                'email' => $user->email,
            ));
            $ids = array();
            foreach ($invites as $i) $ids[] = $i->job_id;
            foreach ($mailinvites as $i) $ids[] = $i->job_id;
            $ids = array_unique($ids);
            $jobs = Job::model()->findAllByAttributes(array('job_id' => $ids));
            $model = User::model()->findByPk(Yii::app()->user->getId());
            Yii::app()->clientScript->registerCssFile('/styles/dev.css');
            Yii::app()->clientScript->registerScriptFile('/js/job.js');
            $this->render('designerMyInvites', array(
                'jobs' => $jobs,
                'model' => $model,
                'pages' => $pages,
                'proposals' => $proposals,
            ));
        } else {
            throw new CHttpException(404);
        }
    }

    /* actions after preview */
    public function actionPostAfterPreview($id = false){

        if(!$id){
            $this->redirect(array('/job'));
        }

        $job = Job::model()->findByPk($id);

        if (empty($job)){
            throw new CHttpException(404);
        }

        if($job->status != Job::STATUS_WAITED) {
            $job->status = Job::STATUS_OPEN;
        }
        $job->save();
        $this->redirect(array('/job/success', 'job' => $id));

    }

    /* for ajax request after "add another question" */
    public function actionGetNewQuestionBlock(){
        $html = $this->renderPartial('_newQuestionBlock', array(), true);
        echo CJSON::encode(
            array(
                'success' => true,
                'html' => $html,
            ));
        Yii::app()->end();
    }

    /* Success page */
    public function actionSuccess($job){
        $job = Job::model()->findByPK($job);
        if (empty($job)){
            throw new CHttpException(404);
        }
        $this->render('success', array(
            'job' => $job,
        ));
    }

    /* Client edits job*/
    public function actionEdit($id){
        $job = Job::model()->findByPk($id);

        if (empty($job) or $job->client_id != Yii::app()->user->getId()){
            throw new CHttpException(404);
        }
        /* getting subcategory */
        if($catId =  Yii::app()->request->getParam('catId', false)){
            $subCats = Job::getSubCategories($catId);
            $html = '<option value="">'. Yii::t('front', 'Please select...') . '</option>';
            foreach ($subCats as $value => $title){
                $html .= '<option value="' . $value . '">' . $title . '</option>';
            }
            echo CJSON::encode(
                array(
                    'success' => true,
                    'html' => $html,
                ));
            Yii::app()->end();
        }

        $this->initSave($job);
        if($job->visibility == Job::GOOGLE_VISIBLE) $job->visibility = Job::GOOGLE_INVISIBLE;
        Yii::app()->clientScript->registerScriptFile('/js/job.js');
        Yii::app()->clientScript->registerCssFile('/styles/dev.css');
        $this->render('editPost', array(
            'job' => $job,
        ));

    }

    public function actionDeleteJob($id){
        $job = Job::model()->findByPk($id);
        $question = JobQuestion::model()->findByPk($job->getPrimaryKey());
        $apps = JobDesigners::model()->findByPk($job->getPrimaryKey());
        if(!empty($job) && $job->delete()){
            if(!empty($question)){
                $question->delete();
            }
            if(!empty($apps)){
                $apps->delete();
            }
            Yii::app()->user->setFlash('success', "Job was deleted successfully!");
            $this->redirect(array('/job'));
        } else {
            throw new CHttpException(404);
        }
    }

    /* Client posts new job*/
    public function actionPostJob($id = false){
        if($id){
            $job = Job::model()->findByPk($id);
            unset($job->job_id);
            unset($job->file);
            unset($job->filename);
            $job->isNewRecord = true;
        } else {
            $job = new Job;
        }
        /* getting subcategory */
        if($catId =  Yii::app()->request->getParam('catId', false)){
            $subCats = Job::getSubCategories($catId);
            $html = '<option value="">'. Yii::t('front', 'Please select...') . '</option>';
            foreach ($subCats as $value => $title){
                $html .= '<option value="' . $value . '">' . $title . '</option>';
            }
            echo CJSON::encode(
                array(
                    'success' => true,
                    'html' => $html,
                ));
            Yii::app()->end();
        }

        $this->initSave($job);
        Yii::app()->clientScript->registerScriptFile('/js/job.js');
        Yii::app()->clientScript->registerScriptFile('/js/arcbazar.js');
        Yii::app()->clientScript->registerCssFile('/styles/dev.css');

        $this->render('postJob', array(
            'job' => $job,
        ));
    }

    /* saving job after create an edit actions */
    public function initSave($job){

        /* ajax validation */
        if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getParam('ajax') == 'postJob-form') {
            echo CActiveForm::validate($job);
            Yii::app()->end();
        }
        if(isset($_POST['Job'])){
            $job->attributes = $_POST['Job'];
            $job->client_id = Yii::app()->user->getId();
            $job->pay_type = Job::PAY_FIXED;
            if($job->isNewrecord) {
                $job->created_at = time();
            } else {
                $job->updated_at = time();
            }

            // If "post job" - job will be opened. In another way it will be draft
            switch($_POST['submit']){
                case (Yii::t('front', Yii::t('front', 'Post Job'))):
                    $job->status = Job::STATUS_OPEN;
                    break;
                case (Yii::t('front', Yii::t('front', 'Save Draft'))):
                    $job->status = Job::STATUS_DRAFT;
                    break;
                case (Yii::t('front', Yii::t('front', 'Preview'))):
                    $job->status = Job::STATUS_DRAFT;
                    break;
            }

            if (isset($_FILES['Job']['name']['doc']) && $_FILES['Job']['name']['doc'] != ''){
                $job->file = md5(time()) . $_FILES['Job']['name']['doc'];
                $job->filename = $_FILES['Job']['name']['doc'];
                $job->doc = CUploadedFile::getInstance($job,'doc');
            } else {
                $job->file = '';
            }

            if (isset($_FILES['Job']['name']['thumb']) && $_FILES['Job']['name']['thumb'] != ''){
                $job->thumbnail = md5(time()) . $_FILES['Job']['name']['thumb'];
                $job->thumbnail_name = $_FILES['Job']['name']['thumb'];
                $job->thumb = CUploadedFile::getInstance($job,'thumb');
            } else {
                $job->thumbnail = '';
            }

            if($job->validate()){

                if($job->save()){
                    if ($job->file != '') {
                        $dir = WATSON . '/../uploadFiles/jobPostFiles';
                        if(!is_dir($dir)){
                            mkdir($dir, 0777);
                        }
                        $dir .= '/' . $job->getPrimaryKey();
                        if(!is_dir($dir)){
                            mkdir($dir, 0777);
                        }
                        $job->doc->saveAs($dir . '/' . $job->file);
                    }

                    if ($job->thumbnail != '') {
                        $dir = WATSON . '/../uploadFiles/jobPostFiles';
                        if(!is_dir($dir)){
                            mkdir($dir, 0777);
                        }
                        $dir .= '/' . $job->getPrimaryKey();
                        if(!is_dir($dir)){
                            mkdir($dir, 0777);
                        }
                        $job->thumb->saveAs($dir . '/' . $job->thumbnail);
                    }
                    $invites = InvDes::model()->findAllByAttributes(array('job_id' => $job->temp_id));
                    if(!empty($invites)){
                        foreach($invites as $i){
                            if($i->job_id != $job->job_id) {
                                $i->job_id = $job->job_id;
                                $i->save();
                            }
                        }
                    }

                    $invites = InvMailDes::model()->findAllByAttributes(array('job_id' => $job->temp_id));
                    if(!empty($invites)){
                        foreach($invites as $i){
                            if($i->job_id != $job->job_id) {
                                $i->job_id = $job->job_id;
                                $i->save();
                            }
                        }
                    }

                    /* Editing exist questions*/
                    if(isset($_POST['QuestionsEdit'])){
                        foreach($_POST['QuestionsEdit'] as $id => $value){
                            $questions = JobQuestion::model()->findByPk($id);
                            $questions->value = $value;
                            $questions->save();

                        }
                    }
                    /* saving questions */
                    if(isset($_POST['Questions'])){
                        foreach($_POST['Questions'] as $q){
                            if (!empty($q)) {
                                $questions = new JobQuestion();
                                $questions->value = $q;
                                $questions->job_id = $job->getPrimaryKey();
                                $questions->save();
                            }
                        }
                    }
                }
            }
            /* redirecting to jobs grid */
            switch($_POST['submit']){
                case (Yii::t('front', Yii::t('front', 'Post Job'))):
                    $this->redirect(array('/job/success', 'job' => $job->getPrimaryKey()));
                    break;
                case (Yii::t('front', Yii::t('front', 'Save Draft'))):
                    $this->redirect(array('/job/list'));
                    break;
                case (Yii::t('front', Yii::t('front', 'Preview'))):
                    $this->redirect(array('/job/preview', 'job' => $job->getPrimaryKey()));
                    break;
            }
        }

        Yii::app()->clientScript->registerScriptFile('/js/job.js');
    }

    public function actionGetSubCategories(){
        $catId =  Yii::app()->request->getParam('catId', false);
        if($catId){
            $subCats = Job::getSubCategories($catId);
            $html = '<option value="">Please Select...</option>';
            foreach ($subCats as $value => $title){
                $html .= '<option value="' . $value . '">' . $title . '</option>';
            }
            echo CJSON::encode(
                array(
                    'success' => true,
                    'html' => $html,
                ));
            Yii::app()->end();
        } else {
            echo CJSON::encode(
                array(
                    'success' => false,
                ));
            Yii::app()->end();
        }
    }

    public function actionDeclineApplicant(){
        if(Yii::app()->getRequest()->isAjaxRequest) {
            $id = Yii::app()->request->getParam('id', false);
            $applicant = JobDesigners::model()->findByPk($id);
            $success = false;
            if(!empty($applicant)){
                $applicant->status = JobDesigners::STATUS_DECLINED;
                if($applicant->save()){
                    $success = true;
                }
            }
            echo CJSON::encode(
                array(
                    'success' => $success,
                    'text' => Yii::t('front', 'Declined by Client'),
                ));
            Yii::app()->end();
        } else {
            throw new CHttpException(404);
        }
    }

    public function sendMessage(Message $newMessage, $job){
        if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getParam('ajax') == 'newMsg-form') {
            echo CActiveForm::validate($newMessage);
            Yii::app()->end();
        }
        if(!$newMessage){
            $newMessage = new Message;
        }
        if(isset($_POST['Message'])){

        $chat = Chat::model()->findByAttributes(array('job_id' => $job->job_id));
        if($chat === null){
            $chat = new Chat;
            $chat->client_id = $job->client_id;
            $chat->designer_id = $job->allow_designer_id;
            $chat->job_id = $job->job_id;
            $chat->updated_at = time();
            $chat->save();
        }

        $id = $chat->chat_id;
            $newMessage->attributes = $_POST['Message'];
            if(empty($newMessage->subject)){
                $newMessage->subject = 'No subject';
            }
            $newMessage->chat_id = $id;
            $newMessage->created_at = time();
            $newMessage->sender = Yii::app()->user->getId();

            if (isset($_FILES['Message']['name']['attach']) && $_FILES['Message']['name']['attach'] != ''){
                $newMessage->file = md5(time()) . $_FILES['Message']['name']['attach'];
                $newMessage->filename = $_FILES['Message']['name']['attach'];
                $newMessage->attach = CUploadedFile::getInstance($newMessage,'attach');
            } else {
                $newMessage->file = '';
            }

            if($newMessage->validate()){
                if($newMessage->save()){
                    if ($newMessage->file != '') {
                        $dir = WATSON . '/../uploadFiles/messageFiles';
                        if(!is_dir($dir)){
                            mkdir($dir, 0777);
                        }
                        $dir .= '/' . $newMessage->chat_id;
                        if(!is_dir($dir)){
                            mkdir($dir, 0777);
                        }
                        $newMessage->attach->saveAs($dir . '/' . $newMessage->file);
                    }
                    Yii::app()->user->setFlash('success', 'Message successfully sent');
                }
            } else {
                CVarDumper::dump($newMessage->getErrors());
            }
        }

    }

    public function checkJobAccess($job){
        if(empty($job)) return false;

        if($job->visibility == Job::GOOGLE_INVISIBLE) {
            return true;
        } elseif($job->client_id == Yii::app()->user->getId()) {
            return true;
        } elseif($job->allow_designer_id == Yii::app()->user->getId()){
            return true;
        } else{
            if(InvDes::checkAccess($job->job_id)){
                return true;
            }
            if(InvMailDes::checkAccess($job->job_id)){
                return true;
            }
            return false;
        }
    }

    public function actionDesChat($des_id){
        $chat = Chat::model()->findByAttributes(array(
            'client_id' => Yii::app()->user->getId(),
            'designer_id' => $des_id,
            'job_id' => null,
        ));
        if(empty($chat)){
            $chat = new Chat;
            $chat->client_id = Yii::app()->user->getId();
            $chat->designer_id = $des_id;
            $chat->updated_at = time();
            $chat->save();
        }
        $newMessage = new Message();
        if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getParam('ajax') == 'newMsg-form') {
            echo CActiveForm::validate($newMessage);
            Yii::app()->end();
        }
        $this->sendMessage($newMessage, $chat->chat_id);
        $newMessage = new Message();

        $messages = Message::model()->findAllByAttributes(array('chat_id' => $chat->chat_id));
        Yii::app()->clientScript->registerScriptFile('/js/job.js');
        $this->render('chat', array(
            'chat' => $chat,
            'messages' => $messages,
            'newMessage' => $newMessage,
            'companion' => $chat->designer,
        ));
    }

    public function sendDirectMessage($newMessage = false, $id){
        if(!$newMessage){
            $newMessage = new Message;
        }
        if(isset($_POST['Message'])){

            $newMessage->attributes = $_POST['Message'];
            if(empty($newMessage->subject)){
                $newMessage->subject = 'No subject';
            }
            $newMessage->chat_id = $id;
            $newMessage->created_at = time();
            $newMessage->sender = Yii::app()->user->getId();

            if (isset($_FILES['Message']['name']['attach']) && $_FILES['Message']['name']['attach'] != ''){
                $newMessage->file = md5(time()) . $_FILES['Message']['name']['attach'];
                $newMessage->filename = $_FILES['Message']['name']['attach'];
                $newMessage->attach = CUploadedFile::getInstance($newMessage,'attach');
            } else {
                $newMessage->file = '';
            }

            if($newMessage->validate()){
                if($newMessage->save()){
                    if ($newMessage->file != '') {
                        $dir = WATSON . '/../uploadFiles/messageFiles';
                        if(!is_dir($dir)){
                            mkdir($dir, 0777);
                        }
                        $dir .= '/' . $newMessage->chat_id;
                        if(!is_dir($dir)){
                            mkdir($dir, 0777);
                        }
                        $newMessage->attach->saveAs($dir . '/' . $newMessage->file);
                    }
                }
            } else {
                CVarDumper::dump($newMessage->getErrors());
            }
        }

    }


    public function actionApplicants($job_id){
        $job = Job::model()->findByPk($job_id);

        $show = false;
        if(empty($job) or Yii::app()->user->getId() != $job->client_id){
            throw new CHttpException(404);
        }

        /* Chat params:  $chat, $messages, $newMessage, $companion */
        $des_id = $job->allow_designer_id;
        $chat = false;
        $messages = false;
        $newMessage = false;
        $companion = false;
        if (!is_null($des_id)) {

            $chat = Chat::model()->findByAttributes(array(
                'client_id' => Yii::app()->user->getId(),
                'designer_id' => $des_id,
                'job_id' => null,
            ));
            if (empty($chat)) {
                $chat = new Chat;
                $chat->client_id = Yii::app()->user->getId();
                $chat->designer_id = $des_id;
                $chat->updated_at = time();
                $chat->save();
            }
            $newMessage = new Message();
            if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getParam('ajax') == 'newMsg-form') {
                echo CActiveForm::validate($newMessage);
                Yii::app()->end();
            }
            $this->sendDirectMessage($newMessage, $chat->chat_id);
            $newMessage = new Message();

            $messages = Message::model()->findAllByAttributes(array('chat_id' => $chat->chat_id));
            $companion = $chat->designer;
        }
        /* End chat params */

        /* Region time params */
        $date = false;
        if (!is_null($des_id)) {
            $params = User::getRegionParams($job->allowDesigner->geo_region);
            $date = User::getTimeSpan($params);
        }
        /* End region time params */

        $apps = JobDesigners::model()->findAllByAttributes(array('job_id' => $job_id));
        $days = Job::getDateAgo($job->created_at);
        $declined = JobDesigners::model()->findAllByAttributes(
            array('job_id' => $job_id, 'status' => JobDesigners::STATUS_DECLINED)
        );
        $shortlist = Shortlist::model()->findByAttributes(array('client_id' => Yii::app()->user->getId()));
        $shortlistedDesigners = array();
        $applicants = array();
        $chats = Chat::model()->findAllByAttributes(array('job_id' => $job_id));
        $messaged = 0;
        if(!is_null($chats)){
            foreach ($chats as $c){
                if(!is_null($c->messages))
                $messaged += count($c->messages);
            }
        }
        $message = new Message;
        $this->sendMessage($message, $job);
        if(isset($_POST['Message'])){
            $show = true;
        }

        if(count($shortlist) > 1){
            foreach($shortlist as $des){
                $shortlistedDesigners[] = $des->designer_id;
            }
        } elseif(count($shortlist)== 1){
            $shortlistedDesigners[] = $shortlist->designer_id;
        }
        foreach ($apps as $app){
//                $app->days_ago = Job::getDateAgo($app->date_apply);
            $applicants[] = $app->designer_id;
        }

        $right_panel = array(
            'Applicants' => count($apps),
            'Shortlisted' => count(array_intersect($applicants,$shortlistedDesigners)) ,
            'Messaged' => $messaged,
            'Declined' => count($declined),
        );
        $midData = false;
        foreach ($job->filesMid as $f){
            if(!$midData) {
                $midData = $f->uploaded_at;
            } else {
                if($f->uploaded_at > $midData)
                    $midData = $f->uploaded_at;
            }
        }
        $midData = $midData ? Yii::app()->dateFormatter->formatDateTime($midData, 'short', null)  : '--';
        $finData = false;
        foreach ($job->filesFin as $f){
            if(!$finData) {
                $finData = $f->uploaded_at;
            } else {
                if($f->uploaded_at > $finData)
                    $finData = $f->uploaded_at;
            }
        }
        $finData = $finData ? Yii::app()->dateFormatter->formatDateTime($finData, 'short', null) : '--';
        Yii::app()->clientScript->registerScriptFile('/js/job.js');
        $this->render('applicants', array(
            'apps' => $apps,
            'date' => $date,
            'job'  => $job,
            'days' => $days,
            'midData' => $midData,
            'finData' => $finData,
            'right_panel' => $right_panel,
            'message'     => $message,
            'chat'       => $chat,
            'messages'   => $messages,
            'newMessage' => $newMessage,
            'companion'  => $companion,
            'show'  => $show,
        ));
    }


    /* Designer Work Room */
    public function actionWorkRoom($id) {
        $job = Job::model()->findByPk($id);

        if(empty($job) or Yii::app()->user->getId() != $job->allow_designer_id){
            throw new CHttpException(404);
        }
        $model = User::model()->findByPk(Yii::app()->user->getId());
        /* Chat params:  $chat, $messages, $newMessage, $companion */
        $client_id = $job->client_id;
        $chat = false;
        $messages = false;
        $newMessage = false;
        $companion = false;
        if (!is_null($client_id)) {
            $chat = Chat::model()->findByAttributes(array(
                'designer_id' => Yii::app()->user->getId(),
                'client_id' => $client_id,
                'job_id' => null,
            ));
            if (empty($chat)) {
                $chat = new Chat;
                $chat->designer = Yii::app()->user->getId();
                $chat->client_id = $client_id;
                $chat->updated_at = time();
                $chat->save();
            }
            $newMessage = new Message();
            if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getParam('ajax') == 'newMsg-form') {
                echo CActiveForm::validate($newMessage);
                Yii::app()->end();
            }
            $this->sendDirectMessage($newMessage, $chat->chat_id);
            $newMessage = new Message();

            $messages = Message::model()->findAllByAttributes(array('chat_id' => $chat->chat_id));
            $companion = $chat->designer;
        }
        /* End chat params */

        $midData = false;
        foreach ($job->filesMid as $f){
            if(!$midData) {
                $midData = $f->uploaded_at;
            } else {
                if($f->uploaded_at > $midData)
                    $midData = $f->uploaded_at;
            }
        }
        $midData = $midData ? Yii::app()->dateFormatter->formatDateTime($midData, 'short', null)  : '--';
        $finData = false;
        foreach ($job->filesFin as $f){
            if(!$finData) {
                $finData = $f->uploaded_at;
            } else {
                if($f->uploaded_at > $finData)
                    $finData = $f->uploaded_at;
            }
        }
        $finData = $finData ? Yii::app()->dateFormatter->formatDateTime($finData, 'short', null) : '--';
        if(isset($_POST['submit']) && $_POST['submit'] == 'Upload'){
            foreach($_FILES['Files']['name'] as $key => $file){
                if($file == '') { continue; }
                $model1 = new JobFiles();
                $model1->job_id = $id;
                $tmp_name = $_FILES['Files']["tmp_name"][$key];
                $model1->file = time() . '_' . $file;
                $model1->file_name = $file;
                $model1->type = $_POST['type'];
                $model1->uploaded_at = time();
                if($model1->validate() && $model1->save()){
                        $dir = WATSON . '/../uploadFiles/jobFiles';
                        if(!is_dir($dir)){
                            mkdir($dir, 0777);
                        }
                        $dir .= '/' . $job->getPrimaryKey();
                        if(!is_dir($dir)){
                            mkdir($dir, 0777);
                        }

                        move_uploaded_file($tmp_name, "$dir/$model1->file");
//                        $model1->doc->saveAs($dir . '/' . $model1->file);
                }else {
                    CVarDumper::dump($model1->getErrors()); die;
                }
            }
            Yii::app()->user->setFlash('success', "Files successfully uploaded!");
            $this->redirect('/job/workRoom/' . $id);
        }
        $this->render('workRoom', array(
            'job'  => $job,
            'chat'       => $chat,
            'messages'   => $messages,
            'newMessage' => $newMessage,
            'companion'  => $companion,
            'model'  => $model,
            'midData'  => $midData,
            'finData'  => $finData,
        ));

    }

    public function actionShortlistAdd(){
        if(Yii::app()->getRequest()->isAjaxRequest) {
            $model = new Shortlist;
            $model->designer_id = Yii::app()->request->getParam('id', false);
            $model->client_id = Yii::app()->user->getId();
            if($model->save()){
                echo CJSON::encode(
                    array(
                        'success' => true,
                        'text' => '<span class="shortisted">' . Yii::t('front','Shortlisted') . '</span>',
                    )
                );
            }else {
                echo CJSON::encode(
                    array(
                        'error' => $model->getErrors(),
                    )
                );
            }
            Yii::app()->end();
        } else {
            throw new CHttpException(404);
        }
    }

    public function actionList(){
        if (User::isDesigner()){
            throw new CHttpException(404);
        }

        $jobs = Job::model()->findAllByAttributes(
            array(
                'client_id' => Yii::app()->user->getId(),
                'paid_status' => Job::STATUS_NOT_PAID,
            ),
            array(
                'order' => 'created_at DESC',
            )
        );
        Yii::app()->clientScript->registerCssFile('/styles/dev.css');
        Yii::app()->clientScript->registerScriptFile('/js/job.js');
        $this->render('jobList', array(
            'jobs' => $jobs,
        ));
    }

    public function actionContracts(){
        if (User::isDesigner()){
            throw new CHttpException(404);
        }

        $contracts = Job::model()->findAllByAttributes(
            array(
                'client_id' => Yii::app()->user->getId(),
                'contract_status' => Job::CONTRACT_STATUS_STARTED,
            ),
            array(
                'order' => 'created_at DESC',
                'limit' => 4,
            )
        );
        Yii::app()->clientScript->registerCssFile('/styles/dev.css');
        Yii::app()->clientScript->registerScriptFile('/js/job.js');
        $this->render('contractsList', array(
            'contracts' => $contracts,
        ));
    }

    public function actionPreparePayment($id) {
        $model = JobDesigners::model()->findByPk($id);
        $this->render('preparePayment', array(
            'model' => $model,
        ));
    }

    public function actionPayJob($id){
        $model = JobDesigners::model()->findByPK($id);
        $cost = $model->flat_rate * (1 + Transaction::PROFIT);
        $return = "http://".$_SERVER['SERVER_NAME'] . Yii::app()->createUrl("job/jobPaymentReturn", array('id' => $id));
        $cancel = "http://".$_SERVER['SERVER_NAME'] . Yii::app()->createUrl("job/jobPaymentError", array('id' => $id));
        $opts = array("description" => "Job award(including 10% to Arcbazar Service)");

        $pp = new PayUtils($cost, $return, $cancel, $opts);
        //initiate paypal redirect
        $pp->sendRequest();

    }

    public function actionJobPaymentReturn($id){
        $model = JobDesigners::model()->findByPk($id);
        $job = Job::model()->findByPk($model->job_id);
        $job->allow_designer_id = $model->designer_id;
        $job->status = Job::STATUS_INWORK;
        $job->contract_status = Job::CONTRACT_STATUS_STARTED;
        $job->paid_status = 1;
        $job->release_budget = $job->budget;
        if($job->validate() && $job->save()){
            Yii::app()->user->setFlash('success', "Designer hired!");
            $this->redirect(Yii::app()->createUrl('/job/applicants', array('job_id' => $model->job_id)));
        }
    }

    public function actionJobPaymentError($id){
        $model = JobDesigners::model()->findByPk($id);
        Yii::app()->user->setFlash('success', "Sorry! service is temporarily unavailable. Please, check your data and try again later");
        $this->redirect(Yii::app()->createUrl('/job/applicants', array('job_id' => $model->job_id)));
    }

    public function actionSendProposal($id){

        $model = JobDesigners::model()->findByPK($id);
        $job = Job::model()->findByPk($model->job_id);

        if($job->status == Job::STATUS_OPEN && $job->client_id = Yii::app()->user->getId()){
            if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getParam('ajax') == 'sendProposal-form') {
                echo CActiveForm::validate($job);
                Yii::app()->end();
            }
            if(isset($_POST['Job'])){
                $job->attributes = $_POST['Job'];
                $job->status = Job::STATUS_WAITED;
                $job->allow_designer_id = $model->designer_id;

                if (isset($_FILES['Job']['name']['cond']) && $_FILES['Job']['name']['cond'] != ''){
                    $job->cond_file = md5(time()) . $_FILES['Job']['name']['cond'];
                    $job->cond_file_name = $_FILES['Job']['name']['cond'];
                    $job->cond = CUploadedFile::getInstance($job,'cond');
                } else {
                    $job->cond_file = '';
                    $job->cond_file_name = '';
                }

                if($job->validate()){
                    if($job->save()){
                        if ($job->cond != '') {
                            $dir = WATSON . '/../uploadFiles/jobPostFiles';
                            if(!is_dir($dir)){
                                mkdir($dir, 0777);
                            }
                            $dir .= '/' . $job->getPrimaryKey();
                            if(!is_dir($dir)){
                                mkdir($dir, 0777);
                            }
                            $job->cond->saveAs($dir . '/' . $job->cond_file);
                        }
                        Yii::app()->user->setFlash('success', "Proposal successfully sent to " . $job->allowDesigner->displayName . "!");
                        $this->redirect(array('/job/index'));
                    }
                }
            }

            $this->render('sendProposal', array(
                'model' => $model,
                'job' => $job,
            ));
        } else {
            throw new CHttpException(404);
        }
    }

    public function actionProposalSent(){
        $this->render('proposalSent');
    }

    public function actionView($id, $workroom = false){
        $job = Job::model()->findByPk($id);
        $apps = JobDesigners::model()->findAllByAttributes(array('job_id' => $id));
        $app = new JobDesigners();
        if (empty($job) && !$this->checkJobAccess($job)){
            throw new CHttpException(404);
        }

        $applied = false;

        if (User::isDesigner()){
            if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getParam('ajax') == 'becomeApp-form') {
                echo CActiveForm::validate($app);
                Yii::app()->end();
            }
            /* Add MetaTag that forbidden google */
            if($job->visibility != Job::GOOGLE_VISIBLE){
                Yii::app()->clientScript->registerMetaTag('noindex', 'googlebot');
            }
            $applied = JobDesigners::getApplied($id, Yii::app()->user->getId());

            if(isset($_POST['JobDesigners']) && isset($_POST['submit'])){
                $app->attributes = $_POST['JobDesigners'];
                $app->job_id = $job->getPrimaryKey();
                $app->designer_id = Yii::app()->user->getId();
                $app->date_apply = time();
                $app->dead_line = $_POST['JobDesigners']['dead_line'];
                $app->letter = ''; //$_POST['JobDesigners']['letter'];
                if($app->validate()){
                    if($app->save()){
                        if(isset($_POST['Answers'])){
                            foreach($_POST['Answers'] as $key => $value){
                                $answer = new Answer();
                                $answer->question_id = $key;
                                $answer->value = $value;
                                $answer->designer_id = Yii::app()->user->getId();
                                if($answer->validate()) {
                                    $answer->save();
                                }

                            }
                        }
                        Yii::app()->user->setFlash('success', "You've successfully applied this job!");
                        $this->sendEmail($job->client->email,'new applicant', $app->letter);

                        $this->redirect(array('/job/myHome'));
                    }
                }
            }
        }

        Yii::app()->clientScript->registerScriptFile('/js/job.js');
        $this->render('view', array(
            'job' => $job,
            'app' => $app,
            'apps' => $apps,
            'applied' => $applied,
            'workroom' => $workroom,
        ));
    }

    public function actionCreateInvoice($id){
        $invoice = new Invoice();
        $job = Job::model()->findByPk($id);
        $params = User::getRegionParams($job->allowDesigner->geo_region);
        $date = User::getTimeSpan($params);
        $contract = Contracts::model()->findByAttributes(array('job_id' =>$id));
        $alltimes = TimeTrack::getTimeSum($contract->contract_id, TimeTrack::ALL_TIME);
        $all_time_budget = TimeTrack::getBudget($alltimes, $job->budget);
        $alltimes = explode(':', $alltimes);
        $alltimes = $alltimes[0] . ' hours ' . $alltimes[1] . 'minutes';
        if(isset($_POST['submit'])){
            $invoice->job_id = $id;
            $invoice->client_id = $job->client_id;
            $invoice->designer_id = $job->allow_designer_id;
            $invoice->amount = $all_time_budget;
            $invoice->status = Invoice::STATUS_OPEN;
            $invoice->created_at = time();;
            if($invoice->validate() && $invoice->save()){
                $this->redirect(Yii::app()->createUrl('/job/contractHistory', array('id' => $id)));
            }
        }
        $this->render('createInvoice', array(
            'invoice' => $invoice,
            'job' => $job,
            'date' => $date,
            'alltimes' => $alltimes,
            'all_time_budget' => $all_time_budget,
            'contract' => $contract,
        ));
    }

    public function actionPayInvoice($id){
        $invoice = Invoice::model()->findByPk($id);
        $cost = $invoice->amount * (1 + Transaction::PROFIT);
        $return = "http://".$_SERVER['SERVER_NAME'] . Yii::app()->createUrl("job/paymentReturn", array('inv_id' => $id));
        $cancel = "http://".$_SERVER['SERVER_NAME'] . Yii::app()->createUrl("job/paymentError", array('inv_id' => $id));
        $opts = array("description" => "Job award(including 10% to Arcbazar Service)");

        $pp = new PayUtils($cost, $return, $cancel, $opts);

        //initiate paypal redirect
        $pp->sendRequest();

    }

    public function actionPaymentError($inv_id){
        Yii::app()->user->setFlash('success', "Sorry! service is temporarily unavailable. Please, check your data and try again later");
        $this->redirect(Yii::app()->createUrl('/job/viewInvoice', array('id' => $inv_id)));
    }

    public function actionPaymentReturn($inv_id){
        $invoice = Invoice::model()->findByPk($inv_id);
        if($invoice === null){
            Yii::app()->user->setFlash('success', "Sorry! service is temporarily unavailable. Please, check your data and try again later");
            $this->redirect(Yii::app()->createUrl('/job/viewInvoice', array('id' => $inv_id)));
        }
        $invoice->status = Invoice::STATUS_PAID;
        $transaction = new Transaction();
        $transaction->designer_id = $invoice->designer_id;
        $transaction->client_id = $invoice->client_id;
        $transaction->job_id = $invoice->job_id;
        $transaction->amount = $invoice->amount;
        $transaction->invoice_id = $inv_id;
        $transaction->type = Transaction::TYPE_INCOMING;
        $transaction->created_at = time();

        $account = Account::model()->findByAttributes(array('designer_id' => $transaction->designer_id));
        if($account === null){
            $account = new Account;
            $account->designer_id = $transaction->designer_id;
            $account->amount = 0;
        }
        $account->amount += $transaction->amount;
        if($transaction->validate() && $invoice->validate() && $account->validate()){
            if($transaction->save() && $invoice->save() && $account->save()){
                Yii::app()->user->setFlash('success', "Invoice successfully paid! Now You can end this contract!");
                $this->redirect(Yii::app()->createUrl('/job/viewInvoice', array('id' => $inv_id)));
            }
        } else{
            CVarDumper::dump($transaction->getErrors());
            echo '<hr>';
            CVarDumper::dump($invoice->getErrors());
            echo '<hr>';
            CVarDumper::dump($account->getErrors());
        }

    }

    public function actionAcceptProposal($id, $accept = false){
        $proposal = Job::model()->findByPk($id);
        if($accept){
            $proposal->status = Job::STATUS_INWORK;
            $proposal->contract_status = Job::CONTRACT_STATUS_STARTED;
            $contract = new Contracts();
            $contract->job_id = $id;
            $contract->contact_person = $proposal->client->getDisplayName(true);
            $contract->date_start = time();
            $chat = Chat::model()->findByAttributes(
                array(
                    'client_id' => $proposal->client_id,
                    'designer_id' => $proposal->allow_designer_id,
                    'job_id' => $proposal->job_id,
                )
            );
            if(is_null($chat)){
                $chat = new Chat;
                $chat->client_id = $proposal->client_id;
                $chat->designer_id = $proposal->allow_designer_id;
                $chat->job_id = $proposal->job_id;
            }

            $chat->updated_at = time();
            if($chat->validate() && $contract->validate() && $proposal->validate()){
                $chat->save();
                $contract->save();
                $proposal->save();
            } else {
                CVarDumper::dump($chat->getErrors());
                CVarDumper::dump($contract->getErrors());
                CVarDumper::dump($proposal->getErrors()); die;
            }
            Yii::app()->user->setFlash('success', "You've successfully accepted this proposal!");
            $this->redirect('/job/myHome');
        }
        $this->render('applyProposal', array(
            'proposal' => $proposal
        ));
    }

    public function actionDeclineProposal($id){
        $proposal = Job::model()->findByPk($id);
        $applicant = JobDesigners::model()->findByAttributes(
            array(
                'job_id' => $proposal->job_id,
                'designer_id' => Yii::app()->user->getId()
            )
        );
        if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getParam('ajax') == 'decline-form') {
            echo CActiveForm::validate($applicant);
            Yii::app()->end();
        }
        if(isset($_POST['submit']) && $_POST['submit'] == 'Decline'){
            $proposal->status = Job::STATUS_OPEN;
            $proposal->visibility = Job::GOOGLE_INVISIBLE;
            $proposal->allow_designer_id = NULL;

            if($proposal->validate()){
                if($proposal->save()){
                    Yii::app()->user->setFlash('success', "You've successfully declined this proposal!");
                    $this->redirect(array('/job/myHome'));
                }
            }
        }
        $this->render('declineProposal', array(
            'proposal' => $proposal,
            'applicant' => $applicant,
        ));
    }

    public function sendEmail($to, $subject, $message, $headers = false){

        if(empty($headers)) {
            $headers = "Content-type: text/html; charset=utf-8 \r\n";
            $headers .= "From: ArcBazar <info@arcbazar.com>\r\n";
        }

        mail($to, $subject, $message, $headers);
    }

    public function actionMyBank(){
        if (User::isDesigner()){
            $account = Account::model()->findByAttributes(array('designer_id' => Yii::app()->user->getId()));
            if(is_null($account)){
                $account = new Account;
                $account->designer_id = Yii::app()->user->getId();
                $account->amount = 0;
                $account->save();
            }
            $transactions = Transaction::model()->findAllByAttributes(array(
                'designer_id' => Yii::app()->user->getId(),
                'type' => Transaction::TYPE_INCOMING,
            ));
            $amount = 0;
            if(!is_null($transactions)){
                foreach ($transactions as $t ){
                    $amount += $t->amount;
                }
            }
            $withdrawn = 0;
            $outgoings = Transaction::model()->findAllByAttributes(array(
                'designer_id' => Yii::app()->user->getId(),
                'type' => Transaction::TYPE_OUTGOING,
            ));
            if(!is_null($outgoings)){
                foreach ($outgoings as $t ){
                    $withdrawn += $t->amount;
                }
            }
            $model = User::model()->findByPk(Yii::app()->user->getId());
            Yii::app()->clientScript->registerCssFile('/styles/dev.css');
            Yii::app()->clientScript->registerScriptFile('/js/job.js');
            $this->render('myBank', array(
                'model' => $model,
                'account' => $account,
                'transactions' => $transactions,
                'amount' => $amount,
                'withdrawn' => $withdrawn,
            ));
        } else {
            throw new CHttpException(404);
        }
    }

    public function actionViewTransaction ($id, $print=false){
        $transaction = Transaction::model()->findByPk($id);
        $model = User::model()->findByPk(Yii::app()->user->getId());
        if($print){
            Yii::app()->clientScript->reset();
            $html = $this->renderPartial(
                'transPrint',
                array(
                    'transaction' => $transaction
                ),
                true,
                false
            );
            ExportService::exportDetailPdf($html, 'transaction_' . $id . '.pdf', true);
            Yii::app()->end();
        }
        $this->render('viewTransaction', array(
            'transaction' => $transaction,
            'model' => $model,
        ));
    }

    public function actionWithdraw(){
        $newRequest = new Withdraw();
        if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getParam('ajax') == 'newRequest-form') {
            echo CActiveForm::validate($newRequest);
            Yii::app()->end();
        }
        if(isset($_POST['Withdraw'])){
            $newRequest->attributes = $_POST['Withdraw'];
            $newRequest->designer_id = Yii::app()->user->getId();
            $newRequest->status = Withdraw::STATUS_OPEN;
            $newRequest->created_at = time();

            if($newRequest->validate()&& $newRequest->save()){
                Yii::app()->user->setFlash('success', "Withdraw request successfully sent!");
                $newRequest = new Withdraw();
            }
        }
        $model = User::model()->findByPk(Yii::app()->user->getId());
        $newRequest->amount = Account::getCurrentAccountAmount();
        $requests = Withdraw::model()->findAllByAttributes(array('designer_id' => Yii::app()->user->getId()));


        $this->render('withdraw', array(
            'requests' => $requests,
            'model' => $model,
            'newRequest' => $newRequest,
        ));
    }

    public function actionPayments(){
        if (!User::isClient()){
            throw new CHttpException(404);
        }

        $payments = Transaction::model()->findAllByAttributes(
            array(
            'client_id' => Yii::app()->user->getId(),
            'type' => Transaction::TYPE_INCOMING,
            ),
            array('order' => 'created_at desc')
        );
        Yii::app()->clientScript->registerCssFile('/styles/dev.css');
        Yii::app()->clientScript->registerScriptFile('/js/job.js');
        $this->render('payments', array(
            'payments' => $payments,
        ));
    }

    public function actionViewPayment($id){
        $payment = Transaction::model()->findByPk($id);
        $this->render('viewPayment', array(
            'payment' => $payment,
        ));
    }

    public function loadModel($id) {
        $model = Job::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function getTempInvites($temp) {
        return InvDes::model()->findAllByAttributes(array('job_id' => $temp));
    }

    public function getMailTempInvites($temp) {
        return InvMailDes::model()->findAllByAttributes(array('job_id' => $temp));
    }

    public function actionInspirationAjax() {
        if(isset($_POST['InvitedDesigner']['pid'])){
            $id = $_POST['InvitedDesigner']['pid'];
        } else if(isset($_POST['pid'])){
            $id = $_POST['pid'];
        } else if(isset($_POST['ProjectInvitation']['pid'])){
            $id = $_POST['ProjectInvitation']['pid'];
        } else {
            $id = 0;
        }

        $job = $_POST['newRecord'] ? new Job : $this->loadModel($id);

        //check if project belongs to user
        if(!$_POST['newRecord']) {
            if (User::GetLoggedInUserId() != $job->client_id) {
                echo json_encode(array('error' => 'You are not authorized for this action'));
                Yii::app()->end();
            }
        }

        //Invite designer
        if ($_POST['action'] == "inviteDesigner" && isset($_POST['InvitedDesigner'])) {
            $oDesignerInvitation = new InvDes;
            $oDesignerInvitation->attributes = $_POST['InvitedDesigner'];
            $oDesignerInvitation->job_id = $_POST['InvitedDesigner']['pid'];
            $oDesignerInvitation->designer_id = $_POST['InvitedDesigner']['uid'];
            $oDesignerInvitation->save();
            $invites = $_POST['newRecord'] ? $this->getTempInvites($_POST['InvitedDesigner']['pid']) : $job->aInvitedDesigners;
            $html = $this->renderPartial('_invDesTable', array('aInvitedDesigners' => $invites, 'newRecord' => $_POST['newRecord']), true);
            echo CJSON::encode(
                array(
                    'success' => true,
                    'html' => $html,
                ));
            Yii::app()->end();
        }

        //Disinvite designer
        if ($_POST['action'] == "disInviteDesigner") {
            $oDesignerInvitation = InvDes::model()->findByAttributes(array('designer_id' => $_POST['uid'], 'job_id' => $_POST['pid']));
            if ($oDesignerInvitation) {
                $oDesignerInvitation->delete();
            }
            $invites = $_POST['newRecord'] ? $this->getTempInvites($_POST['pid']) : $job->aInvitedDesigners;
            $html = $this->renderPartial('_invDesTable', array('aInvitedDesigners' => $invites, 'newRecord' => $_POST['newRecord']), true);
            echo CJSON::encode(
                array(
                    'success' => true,
                    'html' => $html,
                ));
            Yii::app()->end();
        }

        //Invite designer by email
        if ($_POST['action'] == "inviteDesignerByEmail") {
            $oProjectInvitation = new InvMailDes;
//            $oProjectInvitation->setAttributes($_POST['ProjectInvitation']);
            $oProjectInvitation->job_id = $_POST['ProjectInvitation']['pid'];
            $oProjectInvitation->email = $_POST['ProjectInvitation']['email'];
            $oProjectInvitation->save();
            $invites = $_POST['newRecord'] ? $this->getMailTempInvites($_POST['ProjectInvitation']['pid']) : $job->aInvitedMailDesigners;
            $html = $this->renderPartial('_invMailDesTable', array('aInvitedDesigners' => $invites, 'newRecord' => $_POST['newRecord']), true);
            echo CJSON::encode(
                array(
                    'success' => true,
                    'html' => $html,
                ));
            Yii::app()->end();
//            $this->renderPartial('_ajaxInvitedDesignerTable', array('oProject' => $job), false, true);
        }

        //Disinvite designer by email
        if ($_POST['action'] == "disinviteDesignerByEmail") {

            $oDesignerInvitation = InvMailDes::model()->findByAttributes(array('email' => $_POST['email'], 'job_id' => $_POST['pid']));
            if ($oDesignerInvitation) {
                $oDesignerInvitation->delete();
            }
            $invites = $_POST['newRecord'] ? $this->getMailTempInvites($_POST['pid']) : $job->aInvitedMailDesigners;
            $html = $this->renderPartial('_invMailDesTable', array('aInvitedDesigners' => $invites, 'newRecord' => $_POST['newRecord']), true);
            echo CJSON::encode(
                array(
                    'success' => true,
                    'html' => $html,
                ));
            Yii::app()->end();
        }

        Yii::app()->end();


    }
}